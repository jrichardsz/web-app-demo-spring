package com.rnasystems.bank.domain.test;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jrichardsz.minisystems.bank.model.Customer;
import com.jrichardsz.minisystems.bank.repository.CustomerRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-international-bank-model-test.xml")
public class CustomerRepositoryTest {
	
	@Inject
	CustomerRepository repo;
	
	@Transactional @Test
	public void testSave() {
		Customer a = new Customer();
		a.setFirstName("John");
		a.setSsn(125l);
		repo.save(a);
	}

}
