package com.rnasystems.bank.domain.test;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jrichardsz.minisystems.bank.model.Author;
import com.jrichardsz.minisystems.bank.repository.AuthorRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext-international-bank-model-test.xml")
public class AuthorRepositoryTest {
	
	@Inject
	AuthorRepository repo;
	
	@Transactional @Test
	public void testSave() {
		Author a = new Author();
		a.setName("John");
		repo.save(a);
	}

}
