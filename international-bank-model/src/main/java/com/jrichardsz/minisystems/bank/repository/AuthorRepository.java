package com.jrichardsz.minisystems.bank.repository;

import org.springframework.data.repository.Repository;

import com.jrichardsz.minisystems.bank.model.Author;

public interface AuthorRepository extends Repository<Author, Long> {
	
	Author save(Author author);
	
	Author findByName(String name);
	
}
