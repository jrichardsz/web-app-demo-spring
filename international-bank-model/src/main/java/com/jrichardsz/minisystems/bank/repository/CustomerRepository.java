package com.jrichardsz.minisystems.bank.repository;

import org.springframework.data.repository.Repository;

import com.jrichardsz.minisystems.bank.model.Customer;

public interface CustomerRepository extends Repository<Customer, Long> {
	
	Customer save(Customer customer);
	
}
