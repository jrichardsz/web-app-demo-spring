package com.jrichardsz.minisystems.bank.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.Repository;

import com.jrichardsz.minisystems.bank.model.Book;

public interface BookRepository extends Repository<Book, String> {
	
	Book save(Book book);
	
	Book findOne(String isbn);
	
	void delete(String isbn);
	
	List<Book> findAll(); 

	List<Book> findByPublishedGreaterThan(Date date);
	
	List<Book> findByCategoriesIn(String[] categories); 

	List<Book> findByPublishedGreaterThanAndCategoriesIn(Date date, String[] categories); 

}
