package com.jrichardsz.minisystems.bank.service;

import java.util.List;

import com.jrichardsz.minisystems.bank.model.Customer;
import com.jrichardsz.minisystems.bank.service.impl.SearchCriteria;


public interface BankService {

	void createCustomer(Customer customer) throws Exception;
	
	List<Customer> findCustomers(SearchCriteria criteria);
}