package com.jrichardsz.minisystems.bank.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.jrichardsz.minisystems.bank.model.Customer;
import com.jrichardsz.minisystems.bank.repository.CustomerRepository;
import com.jrichardsz.minisystems.bank.service.BankService;

@Service("bankService") 
@Transactional 
public class BankServiceImpl implements BankService {
	
	@PersistenceContext(unitName="bank-database-model")
	private EntityManager entityManager;
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public void createCustomer(Customer customer) throws Exception {
		customerRepository.save(customer);
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Customer> findCustomers(SearchCriteria criteria) {
		String pattern = getSearchPattern(criteria);
		return entityManager.createQuery(
				"select c from Customer c where "
						+ "    lower(c.firstName) like " + pattern
						+ " or lower(c.lastName) like " + pattern
						)
				.setMaxResults(criteria.getPageSize()).setFirstResult(
						criteria.getPage() * criteria.getPageSize())
				.getResultList();
	}
	
	private String getSearchPattern(SearchCriteria criteria) {
		if (StringUtils.hasText(criteria.getSearchString())) {
			return "'%"
					+ criteria.getSearchString().toLowerCase()
							.replace('*', '%') + "%'";
		} else {
			return "'%'";
		}
	}
	
}