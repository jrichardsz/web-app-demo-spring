package com.jrichardsz.minisystems.bank.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jrichardsz.minisystems.bank.model.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/applicationContext-international-bank-service-test.xml")
public class BankServiceImplTest {
	
	@Autowired
	BankService bankService;
	
	@Test
	@Transactional
//	@Rollback(true)
	public void testCreateLoanApplication() throws Exception {
		Customer a = new Customer();
		a.setFirstName("John");
		a.setSsn(125l);
		bankService.createCustomer(a);
	}
	
	
}
