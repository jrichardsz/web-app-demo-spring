package com.jrichardsz.minisystems.bank.controller.customer;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jrichardsz.minisystems.bank.model.Customer;
import com.jrichardsz.minisystems.bank.service.BankService;
import com.jrichardsz.minisystems.bank.service.impl.SearchCriteria;

@Controller
public class CustomersController {

	@Autowired
	BankService bankService;
	
	private Logger logger = Logger.getLogger(CustomersController.class);

	@RequestMapping(value="/customers/create-page")
	public ModelAndView customerPage() {
		return new ModelAndView("/customers/create-page", "customer-entity", new Customer());
	}
	
	@RequestMapping(value = "/customers/search-page", method = RequestMethod.GET)
	public void main(SearchCriteria searchCriteria) {
	}	
	
	@RequestMapping(value = "/customers", method = RequestMethod.GET)
	public String list(SearchCriteria criteria, Model model) {
		List<Customer> customers = bankService.findCustomers(criteria);
		logger.info("Rows result of search:"+customers.size());
		model.addAttribute("customerList",customers);
		return "customers/list";
	}	

	@RequestMapping(value = "/customers/home", method = RequestMethod.GET)
	public void home() {
	}
	
	@RequestMapping(value="/customers/process-create-customer")
	public ModelAndView processPerson(@ModelAttribute Customer customer) {
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("customers/create-result-page");
		
		modelAndView.addObject("cus", customer);
		
		try {
			bankService.createCustomer(customer);
		} catch (Exception e) {
			logger.error("Error when try to create customer.",e);
		}
		
		return modelAndView;
	}	


}