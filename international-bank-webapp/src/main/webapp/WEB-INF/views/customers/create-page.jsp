<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div>
<br>
<p>Fill information about customer:</p>

<spring:url var="customerUrl" value="/customers/process-create-customer">
<%-- 	<spring:param name="id" value="${hotel.id}"/> --%>
</spring:url>

<form:form method="POST" commandName="customer-entity" action="${customerUrl}">
<table>
    <tr>
        <td><form:label path="firstName">Name:</form:label></td>
        <td><form:input path="firstName" /></td>
    </tr>
    <tr>
        <td><form:label path="lastName">lastName:</form:label></td>
        <td><form:input path="lastName" /></td>
    </tr>
    <tr>
        <td><form:label path="ssn">ssn:</form:label></td>
        <td><form:input path="ssn" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Submit"/>
        </td>
        <td></td>
        <td></td>
    </tr>
</table>  
</form:form>
</div>