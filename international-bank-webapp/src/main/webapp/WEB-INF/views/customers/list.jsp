<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="customerResults">
<c:if test="${empty customerList}">
	<p>No Customers. Please, change your search criteria.</p>
</c:if>
<c:if test="${not empty customerList}">

	<p>
	<table class="summary">
		<thead>
			<tr>
				<th>Name</th>
				<th>Last Name</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="customer" items="${customerList}">
				<tr>
					<td>${customer.firstName}</td>
					<td>${customer.lastName}</td>
				</tr>
			</c:forEach>
			<c:if test="${empty customerList}">
				<tr>
					<td colspan="5">No Customers found</td>
				</tr>
			</c:if>
		</tbody>
	</table>

	</p>
</c:if>
</div>	

